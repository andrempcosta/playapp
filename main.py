from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.storage.jsonstore import JsonStore
from kivymd.theming import ThemeManager
from widgets.custom_widgets import PlayRoot
from kivy.utils import platform


# from kivy.config import Config
#
#
# Config.set('kivy', 'default_font', [
#     'ProductSans',
#     './fonts/ProductSansRegular.ttf',
#     './fonts/ProductSansItalic',
#     './fonts/ProductSansBold.ttf',
#     './fonts/ProductSansBoldItalic.ttf',
# ])


class Play(App):
    theme_cls = ThemeManager()
    theme_cls.primary_palette = 'Indigo'
    theme_cls.theme_style = 'Dark'
    title = "Play Health Club"
    stored_data = JsonStore('data.json')
    root = ObjectProperty()
    theme_picker = ObjectProperty()

    def __init__(self, **kwargs):
        self.desktop = False
        super().__init__(**kwargs)

    def build(self):
        if platform in ('linux', 'windows', 'macosx'):
            from kivy.core.window import Window
            Window.size = (540, 720)
            print('Detected Desktop Platform')
            self.desktop = True
        else:
            pass
            # from android import hide_loading_screen
            # hide_loading_screen()
        if not self.stored_data.exists('card_available'):
            self.stored_data.put('card_available', value='False')
        self.root = PlayRoot()
        return self.root

    @staticmethod
    def on_pause():
        return True

    def on_resume(self):
        pass

    def on_stop(self):
        pass


if __name__ == '__main__':
    app = Play()
    app.run()
