from kivy import platform


class Error(Exception):
    """Base class for other exceptions"""
    pass


class ApiError(Error):
    """Raised when the API fails"""

    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


dev = False
show_logs = True
if dev:
    if platform != 'android':
        API_URL = "http://127.0.0.1:5000"
    else:
        API_URL = "http://192.168.1.75:5000"
else:
    API_URL = 'http://playrestapi2.herokuapp.com'

endpoints = {
    'register': '/register',
    'confirm_by_code': '/confirmation_code/user/{}',
    'login': '/login',
    'send_new_confirmation': '/confirmation/user/{}',
    'get_course': '/course/{}',
    'get_all_courses': '/courses',
    'get_enrolled_users': '/enrolled_users/{}',
    'enroll_user': '/enroll/',
    'disenroll_user': '/disenroll/'
}

timeout = 3
