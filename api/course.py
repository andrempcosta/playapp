import asyncio
from concurrent.futures.thread import ThreadPoolExecutor
from functools import wraps
from typing import Dict, Union, Any

from requests import Response, get, ConnectTimeout, post

from api.config import show_logs, API_URL, endpoints, timeout, ApiError
from aux_libs.strings import gettext

_DEFAULT_POOL = ThreadPoolExecutor()


def threadpool(f, executor=None):
    @wraps(f)
    def wrap(*args, **kwargs):
        return asyncio.wrap_future((executor or _DEFAULT_POOL).submit(f, *args, **kwargs))

    return wrap


def get_course(_id: int = None, name: str = None) -> Union[Union[Dict[str, Union[int, Any]], Exception, Response], Any]:
    endpoint = 'get_course'
    response = None
    if show_logs and _id:
        print('{}{}'.format(API_URL, endpoints[endpoint].format(_id)))
    elif show_logs and name:
        print('{}{}'.format(API_URL, endpoints[endpoint].format(name)))
    try:
        if _id:
            response = get('{}{}'.format(API_URL,
                                         endpoints[endpoint].format(_id)), timeout=timeout)
        elif name:
            response = get('{}{}'.format(API_URL,
                                         endpoints[endpoint].format(name)), timeout=timeout)
        if response.status_code == 500:
            raise ApiError('GET' + endpoints[endpoint] + '{} message: {}'.format(response.status_code, response.text))
    except (ConnectionError, ConnectTimeout):
        return {"message": gettext('connection_error'), "status_code": 500}
    except ApiError as e:
        return e.message
    except Exception as e:
        return e
    return response


def get_all_courses() -> Union[Union[Dict[str, Union[int, Any]], Exception, Response], Any]:
    endpoint = 'get_all_courses'
    if show_logs:
        print('{}{}'.format(API_URL, endpoints[endpoint]))
    try:
        response = get('{}{}'.format(API_URL,
                                     endpoints[endpoint]), timeout=timeout)
        if response.status_code == 500:
            raise ApiError('GET' + endpoints[endpoint] + '{} message: {}'.format(response.status_code, response.text))
    except (ConnectionError, ConnectTimeout):
        return {"message": gettext('connection_error'), "status_code": 500}
    except ApiError as e:
        return e.message
    except Exception as e:
        return e
    return response


# @threadpool
def get_enrolled_users(_id: int) -> Union[Union[Dict[str, Union[int, Any]], Exception, Response], Any]:
    endpoint = 'get_enrolled_users'
    if show_logs:
        print('{}{}'.format(API_URL, endpoints[endpoint].format(_id)))
    try:
        response = get('{}{}'.format(API_URL,
                                     endpoints[endpoint].format(_id)), timeout=timeout)
        if response.status_code == 500:
            raise ApiError('GET' + endpoints[endpoint] + '{} message: {}'.format(response.status_code, response.text))
    except (ConnectionError, ConnectTimeout):
        return {"message": gettext('connection_error'), "status_code": 500}
    except ApiError as e:
        return e.message
    except Exception as e:
        return e
    return response


def enroll_user(user_id: int, course_id: int) -> Union[Dict[str, Any], Response]:
    headers = {"Content-Type": "application/json"}
    endpoint = 'enroll_user'
    payload = {'user_id': user_id,
               'course_id': course_id}
    if show_logs:
        print('{}{}'.format(API_URL, endpoints[endpoint]))
        print(headers)
    try:
        response = post('{}{}'.format(API_URL, endpoints[endpoint]),
                        headers=headers, json=payload, timeout=timeout)
        if response.status_code == 500:
            raise ApiError('POST' + endpoints[endpoint] + '{} message: {}'.format(response.status_code, response.text))
    except (ConnectionError, ConnectTimeout):
        return {"message": gettext('connection_error'), "status_code": 500}
    except ApiError as e:
        return e.message
    except Exception as e:
        print(e)
        return e
    return response


def disenroll_user(user_id: int, course_id: int) -> Union[Dict[str, Any], Response]:
    headers = {"Content-Type": "application/json"}
    endpoint = 'disenroll_user'
    payload = {'user_id': user_id,
               'course_id': course_id}
    if show_logs:
        print('{}{}'.format(API_URL, endpoints[endpoint]))
    try:
        response = post('{}{}'.format(API_URL, endpoints[endpoint]),
                        headers=headers, json=payload, timeout=timeout)
        if response.status_code == 500:
            raise ApiError('POST' + endpoints[endpoint] + '{} message: {}'.format(response.status_code, response.text))
    except (ConnectionError, ConnectTimeout):
        return {"message": gettext('connection_error'), "status_code": 500}
    except ApiError as e:
        return e.message
    except Exception as e:
        print(e)
        return e
    return response


if __name__ == '__main__':
    # print(get_course(name="Spinning").json())
    # print(get_course(1).json())
    # print(get_all_courses().json())
    # print(get_enrolled_users(1).json())
    # print(disenroll_user(1000, 1).json())
    pass
