import ast
from typing import Union, Dict, Any

from requests import post, get, put, Response, ConnectTimeout, ConnectionError
from api.config import ApiError, API_URL, endpoints, dev, timeout, show_logs
from json import dumps

from aux_libs.strings import gettext


class User:
    def __init__(self, user_id: int, password: str):
        self.user_id = user_id
        self.temporary_password = password

    def register(self) -> Union[Dict[str, Any], Response]:
        headers = {"Content-Type": "application/json"}

        # convert to json by using class attributes
        user_json = ast.literal_eval(dumps(self.__dict__))
        if show_logs:
            print('{}{}'.format(API_URL, endpoints['register']))
            print(user_json)
            print(headers)
        try:
            response = put('{}{}'.format(API_URL, endpoints['register']),
                           headers=headers, json=user_json, timeout=timeout)
            if response.status_code == 500:
                raise ApiError('PUT /register/ {} message: {}'.format(response.status_code, response.text))
        except (ConnectionError, ConnectTimeout):
            return {"message": gettext('connection_error'), "status_code": 500}
        except ApiError as e:
            return e.message
        except Exception as e:
            print(e)
            return e
        return response

    def confirm_by_code(self, code: int) -> Union[Dict[str, Any], Response]:
        headers = {"Content-Type": "application/json"}
        user_json = {'code': code}
        endpoint = endpoints['confirm_by_code']
        if show_logs:
            print('{}{}'.format(API_URL, endpoint.format(self.user_id)))
            print(user_json)
            print(headers)
        try:
            response = post('{}{}'.format(API_URL, endpoint.format(self.user_id)),
                            headers=headers, json=user_json, timeout=timeout)
            if response.status_code == 500:
                raise ApiError('POST {} {} message: {}'.format(endpoint.format(self.user_id),
                                                               response.status_code, response.text))
        except (ConnectionError, ConnectTimeout):
            return {"message": gettext('connection_error'), "status_code": 500}
        except ApiError as e:
            return e.message
        except Exception as e:
            return e
        return response

    def login(self) -> Union[Dict[str, Any], Response]:
        headers = {"Content-Type": "application/json"}
        # convert to json by using class attributes
        user_json = ast.literal_eval(dumps(self.__dict__))
        user_json['id'] = user_json['user_id']
        user_json.pop('user_id')
        user_json['password'] = user_json['temporary_password']
        user_json.pop('temporary_password')
        if show_logs:
            print('{}{}'.format(API_URL, endpoints['login']))
            print(user_json)
            print(headers)
        try:
            response = post('{}{}'.format(API_URL, endpoints['login']),
                            headers=headers, json=user_json, timeout=timeout)
            if response.status_code == 500:
                raise ApiError('POST /login/ {} message: {}'.format(response.status_code, response.text))
        except (ConnectionError, ConnectTimeout):
            return {"message": gettext('connection_error'), "status_code": 500}
        except ApiError as e:
            return e.message
        except Exception as e:
            print(e)
            return e
        return response

    def send_new_confirmation(self) -> Union[Dict[str, Any], Response]:
        if show_logs:
            print('{}{}'.format(API_URL, endpoints['send_new_confirmation']))
        try:
            response = post('{}{}'.format(API_URL,
                                          endpoints['send_new_confirmation'].format(self.user_id)), timeout=timeout)
            if response.status_code == 500:
                raise ApiError('POST /confirmation/user/ {} message: {}'.format(response.status_code, response.text))
        except (ConnectionError, ConnectTimeout):
            return {"message": gettext('connection_error'), "status_code": 500}
        except ApiError as e:
            return e.message
        except Exception as e:
            return e
        return response


if __name__ == '__main__':
    # andre = User(1875, '1234')
    # r = andre.login()
    # print(r.json())
    # print(r.status_code)
    pass
