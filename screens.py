from copy import deepcopy
from queue import Queue
from typing import Dict
from threading import Thread

from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.core.audio import SoundLoader
from kivy.core.window import Window
from kivy.metrics import dp, sp
from kivy.properties import ObjectProperty, ListProperty, BooleanProperty
from kivy.uix.gridlayout import GridLayout
from kivy.utils import get_hex_from_color

from kivymd.button import MDTextButton, MDRaisedButton
from kivymd.icon_definitions import md_icons

from api.course import get_all_courses, get_enrolled_users, enroll_user, disenroll_user
from utils.color_definitions import slot_color
from widgets.dialog import MDInputDialog

from kivymd.label import MDLabel

from api.user import User
from aux_libs.strings import gettext
from widgets.snackbar import Snackbar
from widgets.tabs import MDBottomNavigationItem
from widgets.custom_widgets import ScanCard, GymCard, LoginForm, MapCourse, MoreList, CustomCard, WeekView, DayView

Clock.schedule_once(lambda x: lazy_load())


class AppHolder:
    def __init__(self):
        self.app = App.get_running_app()
        self.stored_data = self.app.stored_data


def lazy_load():
    global app_holder
    app_holder = AppHolder()


def load_courses() -> None:
    courses = get_all_courses().json()['courses']
    # print(courses)
    app_holder.app.root.ids.b_nav.ids.tab_manager.get_screen('schedule').spawn_cards(courses)


class Home(MDBottomNavigationItem):
    first_time = BooleanProperty(True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # self.bind(on_enter=lambda *x: self.play_test())
        #Clock.schedule_once(lambda x: self.delayed_init(), 0)

    def delayed_init(self):
        self.bind(on_enter=lambda *x: self.play_test())

    def play_test(self):
        self.parent.get_screen('profile').delayed_init()
        self.parent.get_screen('more').delayed_init()
        courses = Thread(target=load_courses, ).start()

    def on_enter(self, *args):
        if self.first_time:
            Clock.schedule_once(lambda x: self.play_test(), 0)
            self.first_time = False
        #print(self.parent.get_screen('schedule'))


class Card(MDBottomNavigationItem):
    card_holder = ObjectProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.user_id = None
        self.scanner = None
        self.gymcard = None
        self.scanner_sound = SoundLoader.load('./assets/scanner_sound.wav') if self.user_id is None else None
        self.save_bind = self.fbind('on_pre_enter', self.delayed_init)
        self.uid_get_id = None

    def delayed_init(self, *args) -> None:
        self.user_id = None if not app_holder.stored_data.exists('id') else app_holder.stored_data.get('id')['value']
        if self.user_id is None:
            self.scanner = ScanCard()
            self.uid_get_id = self.scanner.zbarcam.fbind('codes', self.get_id)
            self.card_holder.add_widget(self.scanner)
        else:
            self.card_holder.clear_widgets()
            self.add_gym_card()
        self.unbind_uid('on_pre_enter', self.save_bind)

    def add_gym_card(self) -> None:
        self.gymcard = GymCard('./assets/card.svg')
        self.card_holder.add_widget(self.gymcard)
        self.gymcard.center = app_holder.app.root.center

    def get_id(self, *args) -> None:
        if args[1] != '[]':
            self.user_id = str(args[1])
            app_holder.app.root.user_id = str(args[1])
            self.parent.get_screen('profile').user_id = str(args[1])
            app_holder.stored_data.put('id', value=str(args[1]))
            self.scanner_sound.play()
            self.scanner.zbarcam.stop()
            self.card_holder.clear_widgets()
            self.scanner.zbarcam.unbind_uid('codes', self.uid_get_id)
            self.add_gym_card()
        print('id: ' + self.user_id)


class Schedule(MDBottomNavigationItem):
    container = ObjectProperty()
    map_course = ObjectProperty()
    week_view = ObjectProperty()
    list_day_view = ListProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Clock.schedule_once(lambda x: self.delayed_init())

    def delayed_init(self):
        pass

    def spawn_cards(self, courses: Dict = None) -> None:
        self.map_course = MapCourse()
        self.week_view = WeekView()
        self.list_day_view = []
        for i in range(6):
            j = 0
            filtered_courses = [d for d in courses if d['day_week'] in [i]]
            _day_view = DayView(_index=i)
            self.list_day_view.append(_day_view)
            self.map_course.ids.carousel.add_widget(_day_view)
            for course in filtered_courses:
                self.list_day_view[i].ids.layout_cards.add_widget(
                    CustomCard(time=course['start_time'][0:5], course_name=course['name'],
                               location=course['location'], pos_hint={'center_x': 0.5},
                               size_hint=(None, None),
                               size=(app_holder.app.root.width / 1.2,
                                     app_holder.app.root.height / 5),
                               callback=self.animate, i_index=j, _id=course['id'],
                               slots=course['slots']))
                j += 1
        self.container.add_widget(self.week_view)
        self.container.add_widget(self.map_course)

    def change_day(self, i):
        self.map_course.ids.carousel.load_slide(self.list_day_view[i])

    # this function needs refactoring, I don't like these inner functions TODO
    def animate(self, *args):

        def refresh_users():
            nonlocal users, check_user
            que = Queue()
            t = Thread(target=lambda q, arg1: q.put(get_enrolled_users(arg1)), args=(que, args[0]._id))
            t.start()
            t.join()
            users = que.get().json()['registered users']
            check_user = [d for d in users if str(d['id']) in [app_holder.app.root.user_id]] != []

        def bottom_button_click(*largs):
            nonlocal users, check_user
            if not check_user:
                if enroll_user(int(app_holder.app.root.user_id), args[0]._id).status_code == 200:
                    largs[0].text = gettext('disenroll')
            else:
                if disenroll_user(int(app_holder.app.root.user_id), args[0]._id).status_code == 200:
                    largs[0].text = gettext('enroll')
            refresh_users()

        binded = False
        list_cards = []
        final_save_pos = {}
        save_size = deepcopy([args[0].width, args[0].height])
        users = []
        check_user = BooleanProperty()
        refresh_users()
        print(users, check_user)

        def test3():
            anim4 = Animation(size=(self.width, Window.height - self.parent.parent.ids.tab_bar.height -
                                    self.container.parent.ids.toolbar.height - self.week_view.height), duration=.6,
                              pos_hint={'center': 0.5})
            anim4.bind(on_complete=lambda *args: test4())
            anim4.start(args[0])
            # self.map_course.ids.carousel.scroll_distance = dp(1000)
            for cardy in list_cards:
                args[0].parent.remove_widget(cardy)

        for card in args[0].parent.children:
            if card != args[0]:
                list_cards.append(card)
                anim = Animation(pos=(-app_holder.app.root.width, card.pos[1]))
                if not binded:
                    anim.bind(on_complete=lambda *gargs: test2(args[0]))
                    binded = True
                anim.start(card)
        if not list_cards:
            test3()

        def test2(_card):
            for _card_ in list_cards:
                x = deepcopy([_card_.pos[0], _card_.pos[1]])
                final_save_pos[_card_.i_index] = x
            args[0].parent.parent.do_scroll = False
            first_card_y = [card_.y for card_ in args[0].parent.children if card_.i_index == 0]
            anim_pos = Animation(y=first_card_y[0], duration=.4)
            anim_pos.bind(on_complete=lambda *args: test3())
            if _card.i_index != 0:
                anim_pos.start(_card)
            else:
                test3()

        def test4():
            slots, slots_value = generate_content(len(users), args[0].slots)
            slots.opacity = 0
            args[0].ids.ay.add_widget(slots)
            args[0].ids.ay.add_widget(slots_value)
            close_x = MDTextButton(text=u"{}".format(md_icons['close-circle']))
            close_x.font_style = 'Icon'
            close_x.font_name = './fonts/materialdesignicons-webfont.ttf'
            close_x.color = app_holder.app.theme_cls.text_color
            app_holder.app.theme_cls.bind(text_color=close_x.setter('color'))
            close_x.pos_hint = {'center_x': .95}
            close_x.font_size = sp(30)
            close_x.opacity = 0
            g_layout = GridLayout(cols=3, rows=8, padding=[dp(20), 0], opacity=0, pos_hint={'center_y': .5})
            close_x.on_press = lambda *largs: test6(slots, slots_value, close_x, g_layout, bottom_button)
            args[0].add_widget(close_x, 1)
            for user in users:
                label = MDLabel(text="{} - {}".format(user['id'], user['name']),
                                disabled=True, halign='center', font_style='Subhead')
                label.color = app_holder.app.theme_cls.text_color
                g_layout.add_widget(label)
            bottom_button = MDRaisedButton(text=gettext('enroll') if not check_user else gettext('disenroll'),
                                           on_press=bottom_button_click, size_hint=(.3, .2),
                                           pos_hint={'center_x': .82}, opacity=0)
            args[0].add_widget(g_layout)
            args[0].add_widget(bottom_button)
            base_position = self.container.height - self.container.parent.ids.toolbar.height - self.week_view.height
            base_duration = .3
            anim_a = Animation(center_y=base_position, duration=base_duration)
            anim_b = Animation(center_y=base_position - dp(45), duration=base_duration)
            anim_c = Animation(center_y=base_position - dp(22.5), opacity=1, duration=base_duration)
            anim_d = Animation(opacity=1, duration=.7)
            anim_l = Animation(opacity=1, duration=base_duration, center_y=base_position - dp(200))
            anim_button = Animation(opacity=1, duration=base_duration)
            anim_a.start(args[0].ids.course_label)
            anim_b.start(args[0].ids.location_label)
            anim_b.start(args[0].ids.icon_label)
            anim_c.start(args[0].ids.time_label)
            anim_c.start(slots)
            anim_c.start(slots_value)
            anim_d.start(close_x)
            anim_l.start(g_layout)
            anim_button.start(bottom_button)
            args[0].callback = lambda x: None
            # temp_widget = MDRaisedButton(on_press=lambda x: test6(x, slots, slots_value, close_x))
            # args[0].add_widget(temp_widget)

        def test6(slots, slots_value, close_x, g_layout, bottom_button):
            # self.map_course.ids.carousel.scroll_distance = dp(20)
            anim_e = Animation(opacity=0, duration=.2)
            anim_e.start(close_x)
            anim_e.start(slots)
            anim_e.start(slots_value)
            anim_e.start(g_layout)
            anim_e.bind(on_complete=lambda *largs: test7())
            l = len(list_cards)
            for cardy in list_cards:
                args[0].parent.add_widget(cardy, l - cardy.i_index)
            anim5 = Animation(size=save_size,
                              duration=.6)
            anim5.start(args[0])
            args[0].parent.parent.do_scroll = True
            args[0].callback = self.animate

            def test7():
                args[0].remove_widget(close_x)
                args[0].ids.ay.remove_widget(slots)
                args[0].ids.ay.remove_widget(slots_value)
                args[0].remove_widget(g_layout)
                args[0].remove_widget(bottom_button)

        def generate_content(students, max_slots):
            slots = MDLabel(text=gettext('slots'),
                            theme_text_color='Primary',
                            font_style='Subhead', size_hint=(.4, 1))
            slots.refreshable = True

            slots_value = MDLabel(theme_text_color='Primary',
                                  font_style='Subhead', size_hint=(.4, 1), markup=True)
            if students >= max_slots:
                slots_value.text = '[color={}]{}/{}[/color]'.format(slot_color['red'], students,
                                                                    max_slots)
            elif students / max_slots >= 0.75:
                slots_value.text = '[color={}]{}/{}[/color]'.format(slot_color['yellow'], students,
                                                                    max_slots)
            else:
                slots_value.text = '[color={}]{}/{}[/color]'.format(slot_color['green'], students,
                                                                    max_slots)

            return slots, slots_value


class Profile(MDBottomNavigationItem):
    container = ObjectProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        #Clock.schedule_once(lambda x: self.delayed_init())

    def delayed_init(self) -> None:
        if not app_holder.app.root.access_token:
            self.container.add_widget(LoginForm())

    def thread_dispatch(self, _widget, password_input):
        Thread(target=self.dispatch_event, args=(_widget, password_input,)).start()

    def dispatch_event(self, _widget, password_input):

        def test_error(_response) -> bool:
            if isinstance(_response, Dict):
                if _response['status_code'] != 200:
                    Snackbar(text=_response['message'],
                             end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()
                return True
            return False

        def login():
            _response = user.login()
            if not test_error(_response):
                if _response.status_code != 200:
                    Snackbar(text=_response.json()['message'],
                             end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()
                else:
                    print('Login Successful')
                    self.container.clear_widgets()
                    app_holder.stored_data.put('access_token', value=_response.json()['access_token'])

        def send_code(code: str):
            try:
                _response = user.confirm_by_code(int(code))
                if not test_error(_response):
                    if _response.status_code != 200:
                        Snackbar(text=_response.json()['message'],
                                 end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()
                    else:
                        login()
            except ValueError:
                Snackbar(text=gettext('code_int'),
                         end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()

        def resend_code():
            _response = user.send_new_confirmation()
            if not test_error(_response):
                if _response.status_code == 201:
                    _dialog = MDInputDialog(
                        events_callback=lambda x: send_code(_dialog.text_field.text),
                        title='[color={}]{}[/color]'.format(get_hex_from_color(app_holder.app.theme_cls.text_color),
                                                            gettext('sms_code')), size_hint=(.8, .4))
                    _dialog.text_field.input_type = 'number'
                    _dialog.open()
                else:
                    Snackbar(text=_response.json()['message'],
                             end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()

        def create_code_dialog():
            dialog = MDInputDialog(
                events_callback=lambda x: send_code(dialog.text_field.text),
                title='[color={}]{}[/color]'.format(get_hex_from_color(app_holder.app.theme_cls.text_color),
                                                    gettext('sms_code')), size_hint=(.8, .4))
            dialog.text_field.input_type = 'number'
            dialog.open()
            print(response.json())

        if len(password_input.text) < 4:
            Snackbar(text=gettext('password_too_short'),
                     end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()
        else:
            user = User(app_holder.stored_data.get('id')['value'], password_input.text)
            if _widget.text == gettext('register'):
                response = user.register()
                if not test_error(response):
                    if response.status_code == 201:
                        create_code_dialog()
                    elif response.status_code == 409:
                        create_code_dialog()
                        snackbar = Snackbar(text=response.json()['message'], button_text=gettext('resend_button'),
                                            button_callback=lambda *args: resend_code(), duration=5,
                                            end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height)
                        snackbar.show()
                    else:
                        Snackbar(text=response.json()['message'],
                                 end_pos=app_holder.app.root.ids.b_nav.ids.tab_bar.height).show()

            elif _widget.text == gettext('login'):
                login()
            else:
                print('Something went wrong')
            # print(response.json())


class More(MDBottomNavigationItem):
    list = ObjectProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        #Clock.schedule_once(lambda x: self.delayed_init())

    def delayed_init(self) -> None:
        self.container.add_widget(MoreList())

    @staticmethod
    def pick(*args) -> None:
        if args[0].name == 'language':
            app_holder.app.root.change_language('en-gb')
        elif args[0].name == 'theming':
            app_holder.app.root.theme_picker_open()
