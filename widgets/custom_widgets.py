from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.graphics.context_instructions import Color
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics.svg import Svg
from kivy.graphics.vertex_instructions import Ellipse, Line
from kivy.metrics import dp, sp
from kivy.properties import BooleanProperty, StringProperty, NumericProperty, BoundedNumericProperty, ListProperty, \
    ObjectProperty, DictProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.scatter import Scatter
from kivy.uix.screenmanager import Screen
from kivy.uix.scrollview import ScrollView
from kivy.utils import get_color_from_hex

from kivymd.button import MDIconButton
from kivymd.label import MDLabel
from kivymd.list import ILeftBodyTouch, IRightBodyTouch, ILeftBody
from kivymd.theming import ThemableBehavior

import aux_libs
from aux_libs.strings import gettext, cached_strings
from widgets.picker_test import Picker
from zbarcam.zbarcam.zbarcam import ZBarCam
from utils.week_days import build_list

built_list = build_list()


class PlayRoot(BoxLayout):
    theme_picker = None
    user_id = StringProperty()
    access_token = StringProperty()
    trigger_language = StringProperty(aux_libs.strings.default_locale)
    courses = DictProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.user_id = '' if not App.get_running_app().stored_data.exists('id') \
            else App.get_running_app().stored_data.get('id')['value']
        self.access_token = '' if not App.get_running_app().stored_data.exists('access_token') \
            else App.get_running_app().stored_data.get('access_token')['value']

    def change_language(self, language: str) -> None:
        if aux_libs.strings.default_locale != language:
            aux_libs.strings.default_locale = language
            aux_libs.strings.refresh()
            self.ids['b_nav'].refresh_tabs()
            self.text_refresh()
            self.trigger_language = language

    # check which widgets need to be refreshed
    def text_refresh(self) -> None:
        for screen in self.ids['b_nav'].ids.tab_manager.screens:
            for widget in screen.walk():
                if hasattr(widget, 'text') and hasattr(widget, 'refreshable'):
                    if hasattr(widget, 'force_refresh') and widget.force_refresh:
                        short_days = list(filter(lambda x: widget.text[0:3] in x, cached_strings.values()))
                        widget.text = gettext(
                            list(cached_strings.keys())[list(cached_strings.values()).index(short_days[0])])[0:3]
                    if widget.text in cached_strings.values():
                        widget.text = gettext(
                            list(cached_strings.keys())[list(cached_strings.values()).index(widget.text)])
                    if hasattr(widget, 'hint_text') and hasattr(widget, 'refreshable') and widget.refreshable:
                        widget.hint_text = gettext(list(cached_strings.keys())
                                                   [list(cached_strings.values()).index(widget.hint_text)])
        self.ids['b_nav'].change_screen('more')

    def theme_picker_open(self):
        if not self.theme_picker:
            self.theme_picker = Picker(title=gettext('change_theme'), color_label=gettext('color'),
                                       style_label=gettext('style'), close=gettext('close'))
            self.bind(trigger_language=lambda *x: self.theme_picker.refresh_labels(gettext('change_theme'),
                                                                                   gettext('color'),
                                                                                   gettext('style'),
                                                                                   gettext('close')))
        self.theme_picker.open()


class ScanCard(BoxLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.zbarcam = ZBarCam()
        self.orientation = 'vertical'
        a = MDLabel(font_style='Body2', theme_text_color='Primary', text=gettext('read_card'),
                    halign='center')
        a.height = a.texture_size[1] + dp(4)
        b = BoxLayout(orientation='vertical', size_hint_y=0.1)
        b.add_widget(a)
        self.zbarcam.code_types = ['CODE39', 'EAN13']
        self.zbarcam.size_hint_y = 0.9
        self.add_widget(self.zbarcam)
        self.add_widget(b)


class GymCard(Scatter):

    def __init__(self, filename, **kwargs):
        app = App.get_running_app()
        self.stored_data = app.stored_data
        if self.stored_data.get('card_available')['value'] == 'False' and self.stored_data.exists('id'):
            self.generate_card(self.stored_data.get('id')['value'], filename)
            self.remove_white_bg(filename)
            self.stored_data.put('card_available', value='True')
        super().__init__(**kwargs)
        if not app.desktop:
            with self.canvas:
                svg = Svg(filename=filename)
            self.size = (svg.width, svg.height)
            self.do_rotation = False
            self.do_scale = True
            self.do_translation = True
            self.scale = 2
        else:
            self.add_widget(Image(source='./assets/card.png', allow_stretch=True))

    @staticmethod
    def generate_card(_id, filename: str) -> None:
        import barcode
        # from barcode.writer import ImageWriter
        # writer=ImageWriter()
        import os
        barcode.get_barcode_class('code39')(_id, add_checksum=False). \
            save(os.path.splitext(filename)[0])

    @staticmethod
    def remove_white_bg(filename: str) -> None:
        fo = open(filename, "r+")
        lines = fo.readlines()
        fo.close()
        f = open(filename, "w")
        for line in lines:
            if line.strip() != r'<rect height="100%" style="fill:white" width="100%"/>':
                f.write(line)
        f.close()


class IconLeftSampleWidget(ILeftBodyTouch, MDIconButton):
    pass


class IconRightSampleWidget(IRightBodyTouch, MDIconButton):
    pass


class AvatarSampleWidget(ILeftBody, Image):
    pass


class LoginForm(BoxLayout):
    _id = StringProperty()
    _has_id = BooleanProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._has_id = App.get_running_app().stored_data.exists('id')
        if self._has_id:
            self._id = App.get_running_app().stored_data.get('id')['value']
        Clock.schedule_once(lambda x: self.delayed_init())

    def delayed_init(self):
        App.get_running_app().root.bind(user_id=self.setter('_id'))
        App.get_running_app().root.bind(user_id=lambda *args: setattr(self, '_has_id', True))


class MapCourse(BoxLayout):
    pass


class UserDetails(BoxLayout):
    pass


class MainWidget(BoxLayout):
    pass


class MoreList(ScrollView):
    pass


class CustomCard(BoxLayout, ThemableBehavior):
    _line_color = ListProperty(get_color_from_hex('006db3'))
    _line_width = NumericProperty(dp(8))
    border_radius = BoundedNumericProperty(dp(3), min=0)
    border_color_a = BoundedNumericProperty(0, min=0., max=1.)
    time = StringProperty()
    course_name = StringProperty()
    location = StringProperty()
    callback = ObjectProperty(lambda *x: None)
    i_index = NumericProperty()
    _id = NumericProperty()
    slots = NumericProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.callback(self)
            # print(self.canvas.get_group('colored_border')[0])
            # self.canvas.get_group('colored_border')[0].width = 20
            # self._line_width = self.width
            # anim = Animation(size=(self.width, self.canvas.get_group('colored_border')[0].size[1]), t='in_sine')
            # anim.start(self.canvas.get_group('colored_border')[0])
        return super().on_touch_down(touch)


class WeekView(GridLayout):
    index = NumericProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Clock.schedule_once(lambda x: self.delayed_init())

    def delayed_init(self):
        # self.parent.parent.parent.map_course.ids.carousel.bind(index=self.setter('index'))
        App.get_running_app().root.ids.b_nav.ids.tab_manager.get_screen('schedule'). \
            map_course.ids.carousel.bind(index=self.setter('index'))


class MiniDay(BoxLayout):
    _index = NumericProperty(0)
    b_list = DictProperty(built_list)
    duration = NumericProperty(.3)
    callback = ObjectProperty(lambda *x: None)
    radius = NumericProperty(dp(16))

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.parent.index = self._index
            temp_color = App.get_running_app().theme_cls.primary_color
            temp_color[3] = .4
            temp_color = Color(*temp_color)
            ellipse_group = InstructionGroup()
            elements = [Color(*App.get_running_app().theme_cls.primary_color),
                        Line(circle=(self.ids.day.center_x, self.ids.day.center_y, self.radius), width=dp(2.5)),
                        temp_color,
                        Ellipse(size=(self.height, self.height), pos=(self.ids.day.center_x - self.height / 2,
                                                                      self.ids.day.center_y - self.height / 2))
                        ]
            self.bind(radius=lambda *x: on_radius())
            for item in elements:
                ellipse_group.add(item)
            self.canvas.add(ellipse_group)
            anim = Animation(width=dp(5.5),
                             duration=self.duration)
            anim.bind(on_complete=lambda *largs: animate())
            anim.start(elements[1])
            anim2 = Animation(radius=dp(36))
            anim2.start(self)
            self.callback(self)

            def on_radius():
                elements[1].circle = (self.ids.day.center_x, self.ids.day.center_y, self.radius)

        def animate():
            ellipse_group.remove(elements[3])
            anim3 = Animation(width=.1, duration=self.duration)
            anim3.bind(on_complete=lambda *largs: animate_1())
            anim3.start(elements[1])

        def animate_1():
            self.canvas.remove(ellipse_group)
            anim4 = Animation(radius=dp(16), duration=.4)
            anim4.start(self)

        return super().on_touch_down(touch)


class DayView(Screen):
    _index = NumericProperty(0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
