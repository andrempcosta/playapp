from kivy.lang import Builder
from kivy.metrics import sp
from kivy.properties import BooleanProperty, StringProperty, ListProperty
from kivy.uix.label import Label
from kivymd.theming import ThemableBehavior
from kivymd.ripplebehavior import CircularRippleBehavior
from kivy.uix.behaviors import ToggleButtonBehavior
from kivy.animation import Animation

Builder.load_string('''
<MDClickableIcon>:
    canvas:
        Clear
        Color:
            rgba: self.color
        Rectangle:
            texture: self.texture
            size: self.texture_size
            pos:
                int(self.center_x - self.texture_size[0] / 2.), \
                int(self.center_y - self.texture_size[1] / 2.)

    text: self._radio_icon
    font_name: 'Icons'
    font_size: sp(24)
    color:
        self._color if self.active \
        else self.theme_cls.secondary_text_color
    halign: 'center'
    valign: 'middle'
''')


class MDClickableIcon(ThemableBehavior, CircularRippleBehavior,
                      ToggleButtonBehavior, Label):
    active = BooleanProperty(False)

    pair_icons = ListProperty()
    _radio_icon = StringProperty()
    _color = ListProperty()

    def __init__(self, **kwargs):
        self.check_anim_out = Animation(font_size=0, duration=.1, t='out_quad')
        self.check_anim_in = Animation(font_size=sp(24), duration=.1,
                                       t='out_quad')
        super(MDClickableIcon, self).__init__(**kwargs)
        self.register_event_type('on_active')
        self.check_anim_out.bind(
            on_complete=lambda *x: self.check_anim_in.start(self))
        self._color = self.theme_cls.primary_color

    def on_state(self, *args):
        if self.state == 'down':
            self.check_anim_in.cancel(self)
            self.check_anim_out.start(self)
            self._radio_icon = self.pair_icons[1]
            self.active = True
        else:
            self.check_anim_in.cancel(self)
            self.check_anim_out.start(self)
            self._radio_icon = self.pair_icons[0]
            self.active = False

    def on_active(self, instance, value):
        self.state = 'down' if value else 'normal'
