from kivy.utils import get_color_from_hex

slot_color = {'green': '#43a047', 'yellow': '#ffeb3b', 'red': '#b61827'}
secondary_color = get_color_from_hex('a4a4a4')
