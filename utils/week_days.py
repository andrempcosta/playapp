from datetime import datetime
from datetime import timedelta

week_list = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']

current_day_week = datetime.today().weekday()
current_day = datetime.today()


# build a dict starting with today, where monday -> 0, tuesday -> 1, ...
def build_list(index=current_day_week):
    names = week_list[index:] + week_list[0:index]
    days = []
    x = 0
    while len(days) < 7:
        if (current_day + timedelta(days=x)).weekday() != 6:
            days.append('{:02d}'.format((current_day + timedelta(days=x)).day))
            x += 1
            if len(days) == 6:
                return {'names': names, 'days': days}
        else:
            x += 1
