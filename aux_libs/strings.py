"""
libs.strings

By default, uses `en-gb.json` file inside the `strings` top-level folder.

If language changes, set `libs.strings.default_locale` and run `libs.strings.refresh()`.
"""
import json
from pathlib import Path

default_locale = "pt-pt"
cached_strings = {}


def get_project_root() -> str:
    """Returns project root folder."""
    return str(Path(__file__).parent.parent)


def refresh():
    print("Refreshing...")
    global cached_strings
    with open(get_project_root() + f"/strings/{default_locale}.json", encoding='utf-8') as f:
        try:
            cached_strings = json.load(f)
        except UnicodeDecodeError:
            print("Can't load JSON file")


def gettext(name):
    return cached_strings[name]


refresh()
